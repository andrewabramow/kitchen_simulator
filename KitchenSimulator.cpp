/*
������� 3. ��������� ������ ����� ������-���������

��� ����� �������

��������� ����������� ���������� ������ ������ �����
���������, ����������� � ������ ������-��������.

������-����� ��������� ������ 5�10 ������. ��� 
��������� ����� �� ����: �����, ���, �����, �����,
����. ��������, ��������� ����� ������, ��������� 
��� � �����. ���� ����� ��������, ��� ��������� �����
� ������ 5�15 ������ ���������� ��� ������� ����� ��
������. ����� ������������ ��������� ������� �
������� ������� std::rand() � ��������� ����������.

������ ��������� ������ 30 ������, �������� �������
����� �� ������ � �������� �� �� ����������.

��������� ����������� ����� 10 �������� ��������.
�� ������ �� ������, ���� �� �����, ��� �������, ���
�������� ������, ��������� ��������������� ���������
� �������.

������ � ������������

��� ��������� �������, ����������� ��������� �������
� ���������� ��������� ���� �������� � ������� detach
��� ��.
*/

#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <ctime>

std::mutex mtx;

void Order(std::vector<std::string>& orders_bag) {
    std::this_thread::sleep_for(std::chrono::seconds
    (5 + std::rand()%5));
    std::vector <std::string> menu = {
        "pizza", "steak", "soup", "salad", "sushi" };

        orders_bag.push_back(menu[rand() % 4]);

        std::cout << std::endl << orders_bag[orders_bag.size()-1] << " - new order\n";
        std::cout << "order bag size " << orders_bag.size() << std::endl << std::endl;
}

void Kitchen(std::vector<std::string>& orders_bag,
            std::vector<std::string>& kitchen_bag) {
                std::this_thread::sleep_for(std::chrono::seconds
                (10 + std::rand() % 5));

                mtx.lock();
                kitchen_bag.push_back(orders_bag[0]);
                orders_bag.erase(orders_bag.begin());
                std::cout << std::endl << kitchen_bag[kitchen_bag.size()-1] << " ready\n";
                std::cout << "kitchen bag size " << kitchen_bag.size() << std::endl;
                std::cout << "order bag size " << orders_bag.size() <<
                    std::endl << std::endl;
                    mtx.unlock();
}
void Delivery(std::vector<std::string>& kitchen_bag,
            int& complete_orders) {
                std::this_thread::sleep_for(std::chrono::seconds
                (30));
                std::cout << "\ndelivery list:\n";
                complete_orders += kitchen_bag.size();
                for (int i = 0; i < kitchen_bag.size(); ++i) {
                    std::cout << kitchen_bag[i] << std::endl;
                }
                std::cout << std::endl;
                kitchen_bag.clear();
                std::cout << "\ncomplete orders: " << complete_orders <<
                    std::endl << std::endl;
}

int main()
{
    std::srand(std::time(nullptr));

    std::vector <std::string> orders_bag;
    std::vector <std::string> kitchen_bag;
    int complete_orders = 0;

    while (complete_orders<=10) {

        std::thread order_thread(Order,std::ref(orders_bag));
            order_thread.join();
        
            if (orders_bag.size() > 0) {
                std::thread kitchen_thread(Kitchen, std::ref(orders_bag), std::ref(kitchen_bag));
                kitchen_thread.detach();
            }

        std::thread delivery_thread(Delivery, std::ref(kitchen_bag),
          std::ref(complete_orders));
          delivery_thread.detach();
    }
}

